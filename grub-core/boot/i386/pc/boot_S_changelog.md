# Changelog for boot.S

2009-06-16  Pavel Roskin  <proski@gnu.org>:  commit 3ef17a2ebf4ef28b4aec10bc146e60763480fa61
- include/grub/i386/pc/boot.h:
  - Remove GRUB_BOOT_MACHINE_ROOT_DRIVE.
  - Adjust GRUB_BOOT_MACHINE_DRIVE_CHECK.
- grub-core/boot/i386/pc/boot.S:
	- Remove root_drive.
	- Assert offset of boot_drive_check by using GRUB_BOOT_MACHINE_DRIVE_CHECK.

2023, 1.:
- Remove boot_drive (1 byte at 0x64), unused since 2009-06-16.
- Remove GRUB_BOOT_MACHINE_BOOT_DRIVE (0x64).
- Decrease GRUB_BOOT_MACHINE_DRIVE_CHECK: 0x66 -> 0x65.

2023, 2.:
- Remove kernel_address (2 bytes), replace with relative immediate JMP to stage2.
- Decrease GRUB_BOOT_MACHINE_KERNEL_SECTOR: 0x5c -> 0x5a.
- Decrease GRUB_BOOT_MACHINE_DRIVE_CHECK: 0x65 -> 0x63.

2023, 3.:
- Remove copy_buffer and related code (~30 bytes):
  no need to load at 0x7000:0 then copy to 0x800:0,
  just load directly at 0x800:0.

2022, 4.:
- Move kernel_sector (8 bytes) 0x5a -> 0x1b0,
  just before the Unique Disk ID and partition table (8 bytes unused space).
  - The 0x1b0 address is burnt into the xorriso tool,
    cannot be changed with parameters.
    @see  --grub2-mbr  at  https://www.gnu.org/software/xorriso/man_1_xorrisofs.html
  - The only other tool to edit kernel_sector is
    grub-setup (setup.c/write_rootdev()), which takes
    the address from GRUB_BOOT_MACHINE_KERNEL_SECTOR.
- Change GRUB_BOOT_MACHINE_KERNEL_SECTOR: 0x5a -> 0x1b0.
- Decrease GRUB_BOOT_MACHINE_DRIVE_CHECK: 0x63 -> 0x5b (8 bytes).

2023, 5.:
- Remove unnecessary cli, sti (2 bytes: 0x5a, +1):
  Setting %ss implicitly disables interrupts for the next instruction
  while the stack pointer is initialized.
- Decrease GRUB_BOOT_MACHINE_DRIVE_CHECK: 0x5b -> 0x5a.
