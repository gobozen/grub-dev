### Int 18h

Cite:  BIOS Boot Specification - Stanford Secure Computer Systems Group
http://www.scs.stanford.edu/05au-cs240c/lab/specsbbs101.pdf
Section 6.7 (page 30):
The original INT 18h handler jumped into a ROM-based BASIC interpreter. On
compatible PCs, INT 18h typically displays an error message, waits for a key stroke,
and then executes an IRET instruction. The BIOS Boot Specification redefines INT
18h as the recovery vector for failed boot attempts. The following pseudocode
describes an implementation of the INT 18h process. Note that INT 18h does not, and
cannot return to its caller because the stack has been reset.
Section D.2 (page 43):
D.2 INT 18h on Boot Failure
If an O/S is either not present, or otherwise not able to load, execute an INT 18h
instruction so that control can be returned to the BIOS. Currently, hard drive boot
sectors do this, but floppy diskette boot sectors execute an INT 19h instead of INT
18h. The BIOS Boot Specification defines INT 18h as the recovery vector for failed
boot attempts
